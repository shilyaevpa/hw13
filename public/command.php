<?php

declare(strict_types=1);

require_once dirname(__FILE__) . '/../vendor/autoload.php';

$app = new \Application\App(new \Infrastructure\CliParser(), new \Infrastructure\Redis(), new \Infrastructure\ConsoleOut());

try {
    $app->run();
} catch (Throwable $exception) {
    echo "Error:" . $exception->getMessage();
}