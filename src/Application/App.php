<?php
	declare(strict_types=1);

    namespace Application;

	use Domain\Event;
    use Domain\EventParameters;

    class App
	{
		public const ADD_COMMAND = 'add';
		public const CLEAR_COMMAND = 'clear';
		public const FIND_COMMAND = 'find';

		protected $cliParser;
		protected $repository;
		protected $resultOut;

		/**
		 * @param $cliParser
		 * @param $repository
		 * @param $resultOut
		 */
		public function __construct(CliParserInterface $cliParser, RepositoryInterface $repository, ResultOutInterface $resultOut)
		{
			$this->cliParser = $cliParser;
			$this->repository = $repository;
			$this->resultOut = $resultOut;
		}

		public function run() : void
		{
			$command = $this->cliParser->getCommand();

			switch ($command)
			{
				case self::FIND_COMMAND:
					$params = $this->cliParser->getEventParams();
					$event = $this->repository->getEvents($params);
					$this->resultOut->printEvent($event);
					break;

				case self::CLEAR_COMMAND:
					$this->repository->clear();
					$this->resultOut->sendMessage("Cleared!");
					break;

				case self::ADD_COMMAND:
                    $event=$this->cliParser->getEvent();
                    $this->repository->addEvent($event);
					$this->resultOut->sendMessage("Added!");
					break;

				default:
					$this->resultOut->sendMessage("Unknown command");
					break;
			}
		}

	}