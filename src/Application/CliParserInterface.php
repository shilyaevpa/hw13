<?php
	declare(strict_types=1);

	namespace Application;

	use Domain\Event;
	use Domain\EventParameters;

	interface CliParserInterface
	{
		public function getCommand(): ?string;
		public function getEvent() : Event;
		public function getEventParams() : EventParameters;
	}
