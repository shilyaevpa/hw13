<?php

	namespace Application;

	use Domain\Event;

	interface ResultOutInterface
	{
		public function sendMessage(string $message);
		public function printEvent(?Event $event);
	}