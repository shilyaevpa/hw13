<?php
	declare(strict_types=1);

	namespace Application;

	use Domain\Event;
	use Domain\EventParameters;

	interface RepositoryInterface
	{
		public function clear() : void;
		public function addEvent(Event $event) : void;
		public function getEvents(EventParameters $parameters) : ?Event;
	}