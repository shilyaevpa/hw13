<?php

	namespace Infrastructure;

	use Application\ResultOutInterface;
    use Domain\Event;
    use Domain\EventParameters;

	class ConsoleOut implements ResultOutInterface
	{

		public function sendMessage(string $message)
		{
			echo $message."\n";
		}

		public function printEvent(?Event $event)
		{
            if ($event===null)
                $this->sendMessage("Nothing found");
            else
			    $this->sendMessage($event->getName());
		}
	}