<?php

	namespace Infrastructure;
	use Application\App;
    use Domain\Event;
    use Domain\EventParameters;
    use Application\CliParserInterface;
    use Garden\Cli\Cli;

	class CliParser implements CliParserInterface
	{
		protected Cli $cli;
		protected $args = null;
		protected $event  = null;
		protected $command  = null;
		protected $eventFilter  = null;

		public function __construct()
		{
			// Define the cli options.
			$this->cli = new Cli();

			$this->cli->description('Event managing system (Redis)')
				->opt('command:c', 'Command (add, clear, find)', true)
				->opt('event:e', 'Event to add', false)
				->opt('filter:f', 'Filter to find events', false);
		}

		public function getCommand(): ?string
		{
			$this->args=$this->cli->parse($argv, true);
			$command = $this->args->getOpt('command');
			if (in_array($command, [App::ADD_COMMAND, App::CLEAR_COMMAND, App::FIND_COMMAND]))
				return $command;
			else
				return null;

		}

        public function getEvent(): Event
        {
            $eventJson = $this->args->getOpt('event');
            $eventArr=json_decode($eventJson, true);
            if (is_array($eventArr) && isset($eventArr["priority"])&& isset($eventArr["name"]))
            {
                $parameters = new EventParameters();
                foreach ($eventArr["conditions"] as $key=>$val)
                {
                    $parameters->setParameter($key, $val);
                }
                return new Event( $eventArr["name"], (int)$eventArr["priority"], $parameters);
            }
            else
                new \InvalidArgumentException("Wrong event parameters!");
        }

        public function getEventParams(): EventParameters
        {
            $eventJson = json_decode($this->args->getOpt('filter'), true);
            if (is_array($eventJson))
            {
                $parameters = new EventParameters();
                foreach ($eventJson["conditions"] as $key=>$val)
                {
                    $parameters->setParameter($key, $val);
                }
                return $parameters;
            }
            else
                new \InvalidArgumentException("Wrong event parameters!");
        }
    }