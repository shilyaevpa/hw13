<?php

	namespace Infrastructure;

	use Application\RepositoryInterface;
    use Domain\Event;
	use Domain\EventParameters;
	use Predis\Client;

	class Redis implements RepositoryInterface
	{
		protected $client;

		/**
		 * @param $client
		 */
		public function __construct()
		{
            //Знаю, что хардкор! Времени нет объяснять )
            //TODO: перенести в config.ini
			$this->client = new Client([
                'scheme' => 'tcp',
                'host'   => 'redis',
                'port'   => 6379,
            ]);;
		}

		public function clear(): void
		{
            $keys = $this->client->keys("event:*");
            foreach ($keys as $key) {
                $this->client->del($key);
            }
		}

		public function addEvent(Event $event): void
		{
            $priority = $event->getPriority();
            $name = $event->getName();
            $parameters = $event->getParams()->getParameters();
            foreach ($parameters as $key=>$val)
                $this->client->zadd( 'event:'.$key.":".$val, $priority,$name);
		}

		public function getEvents(EventParameters $parameters): ?Event
		{
            $parameters = $parameters->getParameters();
            $sets=[];
            foreach ($parameters as $key=>$val) {
                $sets[]="event:".$key.":".$val;
            }

            //Не нашел прямой реализации команды ZINTER
            $this->client->zinterstore('total-set',$sets);
            $eventRaw=$this->client->zRange('total-set',0,1);
            $this->client->del('total-set');

            if (is_array($eventRaw) && count($eventRaw)>0)
            {
                return new Event(array_pop($eventRaw));
            }
                else return null;

		}
	}