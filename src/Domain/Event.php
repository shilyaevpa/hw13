<?php
	declare(strict_types=1);

	namespace Domain;

	class Event
	{
		protected ?string $uuid;
		protected string $name;
		protected ?int $priority;
		protected ?EventParameters $params;

		/**
		 * @param string|null $uuid
		 * @param string $name
		 * @param int $priority
		 * @param EventParameters|null $params
		 */
		public function __construct(string $name, int $priority=null, EventParameters $params = null, string $uuid = null)
		{
			$this->uuid = $uuid;
			$this->name = $name;
			$this->priority = $priority;
			$this->params = $params;
		}

		/**
		 * @return string|null
		 */
		public function getUuid(): ?string
		{
			return $this->uuid;
		}

		/**
		 * @param string|null $uuid
		 */
		public function setUuid(?string $uuid): void
		{
			$this->uuid = $uuid;
		}

		/**
		 * @return string
		 */
		public function getName(): string
		{
			return $this->name;
		}

		/**
		 * @param string $name
		 */
		public function setName(string $name): void
		{
			$this->name = $name;
		}

		/**
		 * @return int
		 */
		public function getPriority(): int
		{
			return $this->priority;
		}

		/**
		 * @param int $priority
		 */
		public function setPriority(int $priority): void
		{
			$this->priority = $priority;
		}

		/**
		 * @return EventParameters
		 */
		public function getParams(): EventParameters
		{
			return $this->params;
		}

		/**
		 * @param EventParameters $params
		 */
		public function setParams(EventParameters $params): void
		{
			$this->params = $params;
		}



	}