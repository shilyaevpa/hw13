<?php
	declare(strict_types=1);

	namespace Domain;

	class EventParameters
	{
		protected array $parameters = [];

		/**
		 * @param array $parameters
		 */
		public function __construct(array $parameters = [])
		{
			$this->parameters = $parameters;
		}

		public function setParameter(string $i, string $value)
		{
			$this->parameters[$i]=$value;
		}

		/**
		 * @return array
		 */
		public function getParameters(): array
		{
			return $this->parameters;
		}

		public function getParametersNumber() : int {
			return count($this->parameters);
		}

	}